#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 24 11:20:43 2022

@author: kruthika.kulkarni
"""

import glob
import pandas as pd
import time
from datetime import datetime, timedelta

def main(parsed_file_location,output_location, counters):
    
    """
    This .py file takes all the incoming parsed data (XML to CSV) from the location 
    (parsed_file_location) and converts it into a single dataframe that can be used
    for model building and stores this dataframe in a CSV (output_location)
    
    parsed_file_location : File location where all the parsed CSV are stored.
    output_location : Location and the name of the file to be used for saving the modeling data. 
    counters: The counters/KPI to be pulled from the parsed data. See the end of this file for an example. 
    This list does not have the PRB counters. Please add them if needed. 
    """
    
    start = time.time()
    df = get_modeling_data_nedo3_1(parsed_file_location, counters)
    df = aggregate_over_du(df, 'sum')
    df = get_throughput_value(df)
    df.to_csv(output_location, index = False)
    end = time.time()
    print("Time Taken: ", end - start)
    


def convert_time(s):
    time_new = datetime.strptime(s[-5:], '%H:%M').time()
    return datetime.fromisoformat(s[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)

def get_modeling_data_nedo3_1(file_location, counters):
    if file_location[-1] != "/":
        file_location = file_location + "/"
    files = glob.glob(file_location+"*.csv")
    df = pd.DataFrame()
    
    for file in files:
        temp = pd.read_csv(file)
        temp = temp[temp['counter'].isin(counters)]
        if df.shape[0] == 0:
            df = temp
        else:
            df = df.append(temp).reset_index(drop = True)
            
    df['DateTime'] = df['measTimeStamp'].apply(convert_time)
    
    df.sort_values(by = ['counter','DateTime'], inplace = True)
    df.reset_index(drop = True, inplace = True)
    df = df[df['granularityPeriod'] == 60]
    df['counter'] = df['measInfoId'] + "__" + df['counter']
    df = df[['counter','DateTime','DU','Value']]
    df = df.pivot(index = ['DateTime','DU'], columns = 'counter')
    df.columns = [x[1] for x in df.columns]
    df = df.reset_index()
    
#     final = pd.DataFrame()
#     for i, col in enumerate(df['counter'].unique()):
#         temp = df[df['counter'] == col][['DU','measInfoId','DateTime','Value']]
#         temp.rename(columns = {'Value': (temp['measInfoId'].unique()[0])+"__"+col}, inplace = True)
#         temp = temp.drop(columns = ['measInfoId'])
#         if final.shape[0] == 0:
#             final = temp
#         else:
#             final = final.merge(temp, how = 'left', left_on = ['DateTime','DU'], right_on = \
#                                ['DateTime','DU'])
            
#     return final
    return df

def aggregate_over_du(df, aggregate):
    if (aggregate == 'mean' ) or (aggregate == 'avg'):
        df = df.groupby(['DateTime']).mean().reset_index()
    elif aggregate == 'sum':
        df = df.groupby(['DateTime']).sum().reset_index()
    elif aggregate == 'max':
        df = df.groupby(['DateTime']).max().reset_index()
    elif aggregate == 'min':
        df = df.groupby(['DateTime']).min().reset_index()
        
    return df

def get_throughput_value(df):
    df['Throughput_UL_pcg1'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.UEThpVolUL.pcg1']/df['GDRB.Qci5qiPcg__VS.GDRB.UEULThpTime.pcg1']
    df['Throughput_UL_pcg3'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.UEThpVolUL.pcg3']/df['GDRB.Qci5qiPcg__VS.GDRB.UEULThpTime.pcg3']
    df['Throughput_DL_pcg1'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.UEThpVolDl.pcg1']/df['GDRB.Qci5qiPcg__VS.GDRB.UEDlThpTime.pcg1']
    df['Throughput_DL_pcg3'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.UEThpVolDl.pcg3']/df['GDRB.Qci5qiPcg__VS.GDRB.UEDlThpTime.pcg3']
    
    return df

counters = ['VS.GDRB.UEDlThpTime.pcg1',
     'VS.GDRB.UEDlThpTime.pcg3',
     'VS.GDRB.UEThpVolDl.pcg1',
     'VS.GDRB.UEThpVolDl.pcg3',
     'VS.GDRB.UEThpVolUL.pcg1',
     'VS.GDRB.UEThpVolUL.pcg3',
     'VS.GDRB.UEULThpTime.pcg1',
     'VS.GDRB.UEULThpTime.pcg3']

parsed_file_location = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.1/1_pm/CSV/"
output_location = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.1/Nedo3_1_ModelingData_v2.csv"

if __name__ == '__main__':
    main(parsed_file_location,output_location, counters)