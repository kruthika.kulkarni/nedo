#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 13:51:00 2022

@author: kruthika.kulkarni
"""

from multiprocess import Pool
import pandas as pd
import numpy as np
import glob
import time
from multiprocess import Pool
from datetime import datetime, timedelta

pd.set_option('display.float_format', lambda x: '%.3f' % x)
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
import warnings
warnings.filterwarnings("ignore")








def get_cluster_upf_pod_mapping(file_location,column_details,server_col = '',\
                                server_details = [],\
                                    container_col = '',container_only = False,\
                                        container_string = ''):
    """
    This code helps you get information about all servers and UPF in the data
    
    file_location : Location of parsed CSV files
    column_details : List of columns which specify the server and other objects (UPF) names in the pandas dataframe. 
                    eg : For Cadvisor, ['objectInstanceId','container','pod'] 
                    represents server, UPF names, pods of a particular UPF in the data
    server_col : If you want only certain server information, then give the column name
                in the pandas dataframe that contains the server details. 
                eg : objectInstanceId
    server_details : If you are told the two cluster names (for Cluster 2 and 3 for nedo purpose),
                     use that here to get only the required information
    container_col : If you want only certain container details (eg upf-service-01)
                    Then give the column in the pandas dataframe that contains this information. 
                    eg : 'container'
    container_only : If you only need the UPFs (eg. upf-service-01), then keep this as True
    container_string = If you are looking for all values of the form 'upf-service'
                       Then give 'upf-service' as the container_string
    
    
    """
    if file_location[-1] != "/":
        file_location = file_location +"/"
    files = glob.glob(file_location+'*.csv')
    mapping_df = pd.DataFrame()
    for file in files:
        df = pd.read_csv(file)
        if len([x for x in df.columns if x in column_details]) != len(column_details):
            print("Bad file", file)
        else:

            temp = pd.DataFrame(df.groupby(column_details)\
                                          .size()).reset_index()[column_details]
            if mapping_df.shape[0] == 0:
                mapping_df = temp
            else:
                
                mapping_df = mapping_df.append(temp).reset_index(drop = True)
                    
    if len(server_details) > 0:
        mapping_df = mapping_df[mapping_df[server_col].isin(server_details)]
        
    if container_only:
        try:
            mapping_df = mapping_df[mapping_df[container_col].str.contains(container_string)]
        except:
            pass
                
    mapping_df.drop_duplicates(inplace = True) 
    return mapping_df
            
def change_timeStamp(file, period,period_value, datetime_col):
    """
    data : Pandas Dataframe on which we need to change the time
    period : Could take 'minute' and 'hour'. 
             If the datetime needs to be changed to 15 minutes interval, we give 'minute' as the value. 
    period_value : In the above example of 15 minutes, 15 is the period_value
    datetime_col : The column in Pandas dataframe (data) that represents the time column
    
    """
    data = pd.read_csv(file)
    data[datetime_col] = pd.to_datetime(data[datetime_col])
    
    if (period.lower() == 'minute') and (period_value == 1):
        data[datetime_col] = data[datetime_col].dt.floor(freq='Min')
        
    elif period.lower() == 'minute':
        data[datetime_col] = data[datetime_col].dt.floor(freq='Min')
        series_time = ('00:'+ (data[datetime_col].dt.minute%period_value).astype(str)+":00")
        data[datetime_col] = data[datetime_col] - pd.to_timedelta(series_time.astype(str))
    
    
    elif (period.lower() == 'hour') and (period_value == 1):
        data[datetime_col] = data[datetime_col].dt.floor(freq='H')   
                                   
    elif period.lower() == 'hour':
        data[datetime_col] = data[datetime_col].dt.floor(freq='H')  
        series_time = (data[datetime_col].dt.hour%period_value).astype(str)+":00:00"
        data[datetime_col] = data[datetime_col] - pd.to_timedelta(series_time)

    return data

        
def get_aggregated_data(data, group_by_columns,datetime_col, aggregation_method):
    """
    data : The dataframe on which we need to aggregate the value
    group_by_columns : List of column names on which we need to aggregate the values
                       Include 'performanceMetrics' to this list if you are using this code on the parsed data. 
    datetime_col : The column in Pandas dataframe (data) that represents the time column
    aggregation_method : Could be one of the following - 'sum','mean','avg','min','max'
    """
    if aggregation_method.lower() == 'sum':
        agg_data = data.groupby(group_by_columns+[datetime_col]).sum().reset_index()
    if (aggregation_method.lower() == 'mean') or (aggregation_method.lower() == 'avg'):
        agg_data = data.groupby(group_by_columns+[datetime_col]).mean().reset_index()
    if aggregation_method.lower() == 'min':
        agg_data = data.groupby(group_by_columns+[datetime_col]).min().reset_index()
    if aggregation_method.lower() == 'max':
        agg_data = data.groupby(group_by_columns+[datetime_col]).max().reset_index()
        
    return agg_data  
 
#Example arguemnts for all codes

# get_cluster_upf_pod_mapping
file_location = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/CADVISOR_Data/CSV"
column_details = ['objectInstanceId','container','pod']
server_col = 'objectInstanceId'
server_details = ['uhn7kl5rcrb','uhn7kls21']
container_col = 'container'
container_only = True
container_string = 'upf-service'

# change_timeStamp
files = glob.glob(file_location+"/*.csv")
period = 'minute'
period_value = 15
datetime_col = 'timeStamp'

# Structure for multiprocessing the function change_timeStamp
p = Pool(4)

arguments_change_timeStamp = [(file,period,period_value,datetime_col) for file in files]
p.starmap(change_timeStamp, arguments_change_timeStamp)

# get_aggregated_data
group_by_columns = ['objectInstanceId','container','performanceMetric']
datetime_col = 'timeStamp'
aggregation_method = 'mean'
arguments_get_aggregated_data = [(pd.read_csv(file),group_by_columns, datetime_col,aggregation_method )\
             for file in files]
p = Pool(4)
p.starmap(get_aggregated_data, arguments_get_aggregated_data)
                            
                                   
        
        
    
