#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 12:09:16 2022

@author: kruthika.kulkarni
"""


import pandas as pd
import numpy as np
from datetime import datetime
import plotly.express as px
from neuralprophet import NeuralProphet
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from pyod.models.cblof import CBLOF
import plotly.graph_objects as go
from joblib import dump, load
from pyod.models.cblof import CBLOF
from neuralprophet import NeuralProphet
import time
from neuralprophet import set_random_seed
import os
import math
import pickle

data_path_dl = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.2/CSV_Features/system_upf_n3_dl_throughput_rate.csv"
ns_uuid = "694dfbc0-951f-428a-8006-e1e8d1295c3a"
cnfc_uuid = "0e3653f4-9114-4208-bf30-25f9877fb683"
current_capacity = 25
output_path = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.2/"
model_path = output_path+'/neuralprophet_model2.pkl'

def get_model(data_path_dl, ns_uuid,cnfc_uuid, current_capacity, output_path, model_path):
    """
    data_path_dl : The path where the data is stored. CSV containing 
    """
    df = pd.read_csv(data_path_dl)
    df = preprocess(df, ns_uuid, cnfc_uuid)
    if not os.path.isfile(model_path):
        build_and_save_model(df, output_path)
    return df

def predict_upf(data_path_dl,ns_uuid,cnfc_uuid, current_capacity, model_path):
    current_num_of_upf = current_capacity/5
    num_of_upf = current_num_of_upf
    with open(model_path, "rb") as f:
        model = pickle.load(f)
        df = pd.read_csv(data_path_dl)
        df = preprocess(df, ns_uuid, cnfc_uuid)
        df = df[['timeStamp2','system_upf_n3_dl_throughput_rate']]
        df.columns= ['ds','y']
        print(df.tail(),"DF")
        future = model.make_future_dataframe(df, periods=2)
        df = df.append(future)
        print(future)
        forecast = model.predict(df)
        print(forecast)
        
        fig1 = px.line(forecast, x="ds", y=['y','yhat1'],
                      hover_data={"ds": "|%Y %B, %d, %H"},
                      title='custom tick labels')
        fig1.update_xaxes(
            dtick="M1",
            tickformat="%d %b\n%Y")
        
        fig1.write_html(output_path+"prediction.html")
        
        dl_thp_30min = forecast.tail(1)['yhat1'].values[0]*1e-6
        if (dl_thp_30min/current_capacity)*100 <= 40:
            capacity_needed = dl_thp_30min/0.5
            num_of_upf = math.ceil(capacity_needed/5)
            if num_of_upf != current_num_of_upf:
                print("Send recommendation")
            else:
                print("do nothing")
        if (dl_thp_30min/current_capacity)*100 >= 60:
            capacity_needed = dl_thp_30min/0.5
            num_of_upf = math.ceil(capacity_needed/5)
            if num_of_upf != current_num_of_upf:
                print("Send recommendation")
            else:
                print("do nothing")
                
    return num_of_upf
    
    
    
def preprocess(df, ns_uuid,cnfc_uuid):
    df = df[(df['ns_uuid'] == ns_uuid) & (df['cnfc_uuid'] == cnfc_uuid)]
    df['system_upf_n3_dl_throughput_rate'] =  df['system_upf_n3_dl_throughput_rate'].replace("-",float("NaN"))
    df['system_upf_n3_dl_throughput_rate'] =  df['system_upf_n3_dl_throughput_rate'].astype('float')
    df['timeStamp2'] = df['timeStamp'].str.split(".").str[0].str.replace("T",' ')
    print(df['timeStamp2'])
    df['timeStamp2'] = pd.to_datetime(df['timeStamp2'])
    
    # Only for 15 minutes data, for easier computation
    df['Seconds'] = df['timeStamp2'].dt.second
    for ind in df.index:
        df.loc[ind,'timeStamp2'] = df.loc[ind,'timeStamp2'] - \
            timedelta(seconds = int(df.loc[ind,'Seconds']))
    df.drop(columns = ['Seconds','timeStamp'], inplace = True)
    df.drop_duplicates(inplace = True)
    df.sort_values(by = ['timeStamp2'], inplace = True)
    print(df,"Preprocess done")
    
    return df

def build_and_save_model(train, output_path):
    col = "system_upf_n3_dl_throughput_rate"
    train.dropna(subset = [col], inplace = True)
    train_sub = train[['timeStamp2',col]]
   
    train_sub.columns = ['ds','y']
    print(train_sub.dtypes)
    set_random_seed(0)
    model = NeuralProphet(num_hidden_layers=2)
    model.fit(train_sub, freq = '15min')
    with open(model_path, "wb") as f:
     # dump information to that file
         pickle.dump(model, f)

df = get_model(data_path_dl, ns_uuid,cnfc_uuid, current_capacity, output_path, model_path)
final_upf = predict_upf(data_path_dl,ns_uuid,cnfc_uuid, current_capacity, model_path)