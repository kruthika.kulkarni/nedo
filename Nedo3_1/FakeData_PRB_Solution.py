#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 20 15:41:17 2022

@author: kruthika.kulkarni
"""

import pandas as pd
import numpy as np
from datetime import datetime
import plotly.express as px
from neuralprophet import NeuralProphet
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from pyod.models.cblof import CBLOF
import plotly.graph_objects as go
from joblib import dump, load
from pyod.models.cblof import CBLOF
from neuralprophet import NeuralProphet
import time
from neuralprophet import set_random_seed
import os
import math

def prb_allocation(df,sla, low):
    start = time.time()
    df['Time Stamp'] = df[['measTimeStamp']].apply(convert_time, axis = 1)
    
    df = df.sort_values(by =['Time Stamp']).tail(30).reset_index(drop = True)
    
    df = preprocess(df)
    
    #Currently looking only for pcg1 - downlink
    df['dl_rate_pcg1'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.CELLDlThpVol.pcg1']/df['GDRB__GDRB.UEDlThpTime']
    df['dl_rate_pcg3'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.CELLDlThpVol.pcg3']/df['GDRB__GDRB.UEDlThpTime']
    df['ul_rate_pcg1'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.CELLUlThpVol.pcg1']/df['GDRB__GDRB.UEULThpTime']
    df['ul_rate_pcg3'] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.CELLUlThpVol.pcg3']/df['GDRB__GDRB.UEULThpTime']
    
    print(df['dl_rate_pcg1'])
    
    dlthpvol_5min_future = build_model(df,sla,'dl_rate_pcg1')
    if dlthpvol_5min_future < sla:
        increase_in_throughput = low - dlthpvol_5min_future
        increase_prb_by = increase_in_throughput * 0.00013193712828051823
        print(increase_prb_by)
    else:
        increase_prb_by = 0
        
    end = time.time()
    print("Time Taken: ", (end-start)," Seconds")
        
    return increase_prb_by
    
    
    
    
def build_model(train,sla,col):
    train.dropna(subset = [col], inplace = True)
    train_sub = train[['Time Stamp',col]]
   
    train_sub.columns = ['ds','y']
    # print(train_sub.tail(1)['ds'].values[0])
    # future = [datetime.strptime(str(train_sub.tail(1)['ds'].values[0]).split(".")[0],'%Y-%m-%dT%H:%M:%S') + timedelta(minutes = i) for i in range(1,6)]
    # future = pd.DataFrame(np.array(future))
    # future['y'] = float('NaN')
    # future.columns = ['ds','y']
    # future = train_sub.append(future)
    print(train_sub.dtypes)
    set_random_seed(0)
    model = NeuralProphet(num_hidden_layers=2)
    model.fit(train_sub, freq = '1min')
    future = model.make_future_dataframe(train_sub, periods=5)
    print(future,"Future")
    forecast = model.predict(future)
    print(forecast)
    dlthpvol_5min_future = forecast.tail(1)['yhat1'].values[0]
    
    
    #return forecast
    return dlthpvol_5min_future
    
def preprocess(df):
    for col in df.columns:
        if col not in ['Time Stamp','measTimeStamp','measTimeStamp1']:
            df[col] = df[col].replace("-",float('NaN'))
            df[col] = df[col].astype(float)
    
    return df
    
def convert_time(s):
    s1 = str(s.values[0])
    time_new = datetime.strptime(s1[-5:], '%H:%M').time()
    return datetime.fromisoformat(s1[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)

