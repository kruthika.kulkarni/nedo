#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 26 10:18:59 2022

@author: kruthika.kulkarni
"""

import json
import pandas as pd
from datetime import datetime, timedelta
from neuralprophet import NeuralProphet
import time
from neuralprophet import set_random_seed

api_json = "nedo3_1_request.json"
data_path = "Parser_Example_Ouptutcopy.csv"

def main(data_path, api_json):
    start = time.time()
    # Read the API json
    item = ""
    with open(api_json,'r') as f:
        item = json.loads(f.read())
        
    if (item['Type'].lower() == 'request') and (item['API Type'].lower() == 'start'):
        # Get all the details needed for modeling
        # Data containing the throughput volume and time
        df = pd.read_csv(data_path)
        # Current target PCG ID
        target_pcg = item['TargetPCG']
        # Target PCG information - dictionery
        pcg_info = [x for x in item['SNSSAI_info'] if x['PCG ID'] == target_pcg][0]
        #Target PCG current allocated PRB percentage
        current_dedicated = pcg_info['rRMPolicyDedicatedRatio']
        #Target PCG max allowed PRB percentage
        max_allowed = pcg_info['rRMPolicyMaxRatio']
        # The gauranteed throughput rate (dl + ul)
        sla = item['gauranteed']
        # The value of throughput below which CPE triggers the API
        low = item['low']
        # The value above which, the CPE stops the AI recommendation
        high = item['high']
        
        df = preprocess(df, target_pcg)
        
        # Get the forecasted values 5 minutes into the future
        dlthpvol_5min_future = build_model(df,'throughput_pcg'+str(target_pcg))
        # if the future value will be less than the garunteed throughput the do the below steps
        if dlthpvol_5min_future < sla:
            increase_throughput_by = low - dlthpvol_5min_future + \
                0.1*(high - low)
            # Taking the average throughput rate of the last 5 minutes
            avg_throughput_rate = df['throughput_pcg'+str(target_pcg)].tail(5).mean()

            prb_required = \
                min((increase_throughput_by*current_dedicated)/avg_throughput_rate,\
                    max_allowed)       
            #Change the request to response in the API
            item['Type'] = 'Response'
            for ele in item['SNSSAI_info']:
                if ele['PCG ID'] == target_pcg:
                    ele['rRMPolicyDedicatedRatio'] = prb_required
                else:
                    ele['rRMPolicyDedicatedRatio'] = \
                        max(ele['rRMPolicyDedicatedRatio'] - \
                            (prb_required - current_dedicated), \
                            ele['rRMPolicyMinRatio'])
            print(item)  
            end = time.time()
            print("Time Taken: ", end - start)
            return item
        else:
            print("No breech, Do nothing")
            end = time.time()
            print("Time Taken: ", end - start)
            return -1
        
        
        
        
        
        
    else:
        print("Do Nothing as AI has been asked to stop recommendation")
        end = time.time()
        print("Time Taken: ", end - start)
        return 
        
        
def preprocess(df, pcg_id):
    # The below step is commented out as the parser does this step
    # df['Time Stamp'] = df[['measTimeStamp']].apply(convert_time, axis = 1)
    df = df.sort_values(by =['measTimeStamp']).tail(30).reset_index(drop = True)
    cols = ['GDRB.Qci5qiPcg__VS.GDRB.CELLDlThpVol.pcg'+str(pcg_id), \
            'GDRB.Qci5qiPcg__VS.GDRB.CELLUlThpVol.pcg'+str(pcg_id)]
    for col in cols:
            df[col] = df[col].replace("-",float('NaN'))
            df[col] = df[col].astype(float)
     
    # Get the DL and UL throughput using the columns. 
    #We still need to replace Time with Time+pcg_id        
    df['dl_rate_pcg'+str(pcg_id)] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.CELLDlThpVol.pcg'+str(pcg_id)]/df['GDRB__GDRB.UEDlThpTime']

    df['ul_rate_pcg'+str(pcg_id)] = \
    df['GDRB.Qci5qiPcg__VS.GDRB.CELLUlThpVol.pcg'+str(pcg_id)]/df['GDRB__GDRB.UEULThpTime']
    # Combine the ul and dl throughput for forecasting the value 5 minutes into future.
    df['throughput_pcg'+str(pcg_id)] = df['dl_rate_pcg'+str(pcg_id)] + \
                                        df['ul_rate_pcg'+str(pcg_id)]

    
    return df
    
def convert_time(s):
    s1 = str(s.values[0])
    time_new = datetime.strptime(s1[-5:], '%H:%M').time()
    return datetime.fromisoformat(s1[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)


# This will be a one time activity if we get 5 days of data. 
#If we do not get so much data, then we can choose to predict on the go everytime.
def build_model(train,col):
    train.dropna(subset = [col], inplace = True)
    train_sub = train[['measTimeStamp',col]]
   
    train_sub.columns = ['ds','y']
    set_random_seed(0)
    model = NeuralProphet(num_hidden_layers=2)
    model.fit(train_sub, freq = '1min')
    future = model.make_future_dataframe(train_sub, periods=5)
    print(future,"Future")
    forecast = model.predict(future)
    print(forecast)
    dlthpvol_5min_future = forecast.tail(1)['yhat1'].values[0]
    
    
    #return forecast
    return dlthpvol_5min_future
        
if __name__ == '__main__':
      main(data_path, api_json)   